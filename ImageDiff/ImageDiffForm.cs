﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageDiff
{
    public partial class ImageDiffForm : Form
    {
        private double mu_tolerance = 0.1;
        private bool useMedianFilter = false;
        private short matrixSize = 3;
        private bool greyScale = false;

        public double MuTolerance { get { return mu_tolerance; } set { mu_tolerance = value; } }

        public ImageDiffForm()
        {
            InitializeComponent();
        }

        private void loadFirstButton_Click(object sender, EventArgs e)
        {
            LoadImage("First image", firstPictureBox);
        }

        private void LoadImage(string caption, PictureBox pictureBox)
        {
            openFileDialog.Title = caption;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pictureBox.ImageLocation = openFileDialog.FileName;
            }
        }

        private void loadSecondButton_Click(object sender, EventArgs e)
        {
            LoadImage("Second image", secondPictureBox);
        }

        private void compareButton_Click(object sender, EventArgs e)
        {
            CompareImages();
        }

        private void CompareImages()
        {
            var bmpA = new Bitmap(Image.FromFile(firstPictureBox.ImageLocation));
            var bmpB = new Bitmap(Image.FromFile(secondPictureBox.ImageLocation));

            int width = bmpA.Width;
            int height = bmpA.Height;

            Debug.Assert(width == bmpB.Width && height == bmpB.Height, "Images must be equal dimension");//TODO: Make dimension changes for image validation
            Debug.Assert(bmpA.PixelFormat == bmpB.PixelFormat, "Pixel format of images must be equal");

            Rectangle aRect = new Rectangle(new Point(0, 0), bmpA.Size);

            BitmapData aData = bmpA.LockBits(aRect, ImageLockMode.ReadOnly, bmpA.PixelFormat);
            BitmapData bData = bmpB.LockBits(aRect, ImageLockMode.ReadOnly, bmpB.PixelFormat);

            Bitmap result = bmpA.Clone(aRect, bmpA.PixelFormat);

            BitmapData rData = result.LockBits(aRect, ImageLockMode.ReadWrite, result.PixelFormat);

            int aStride = aData.Stride;
            int bStride = bData.Stride;
            int rStride = rData.Stride;

            IntPtr aScan0 = aData.Scan0;
            IntPtr bScan0 = bData.Scan0;
            IntPtr rScan0 = rData.Scan0;

            int aPixelSize = aStride / width;

            List<Point> diff = new List<Point>();

            unsafe
            {
                for (int y = 0; y < height; y++)
                {
                    byte* aRow = (byte*)aScan0 + (y * aStride);
                    byte* bRow = (byte*)bScan0 + (y * bStride);
                    byte* rRow = (byte*)rScan0 + (y * rStride);

                    for (int x = 0; x < width; x++)
                    {
                        ComparePixels(bmpA.PixelFormat, aPixelSize, aRow, bRow, rRow, x);
                        //TODO: Implement different byte management. 
                    }
                }
            }

            bmpA.UnlockBits(aData);
            bmpB.UnlockBits(bData);
            result.UnlockBits(rData);
            bmpA.Dispose();
            bmpB.Dispose();

            if (useMedianFilter)
            {
                resultPictureBox.Image = MedianFilter(result, matrixSize, greyScale);
            }
            else
            {
                resultPictureBox.Image = result;
            }
        }

        private unsafe void ComparePixels(PixelFormat pixelFormat, int aPixelSize, byte* aRow, byte* bRow, byte* rRow, int x)
        {
            switch (pixelFormat)
            {
                case PixelFormat.Indexed:
                    break;
                case PixelFormat.Gdi:
                    break;
                case PixelFormat.Alpha:
                    break;
                case PixelFormat.PAlpha:
                    break;
                case PixelFormat.Extended:
                    break;
                case PixelFormat.Canonical:
                    break;
                case PixelFormat.Undefined:
                    break;
                case PixelFormat.Format1bppIndexed:
                    break;
                case PixelFormat.Format4bppIndexed:
                    break;
                case PixelFormat.Format8bppIndexed:
                    break;
                case PixelFormat.Format16bppGrayScale:
                    break;
                case PixelFormat.Format16bppRgb555:
                    break;
                case PixelFormat.Format16bppRgb565:
                    break;
                case PixelFormat.Format16bppArgb1555:
                    break;
                case PixelFormat.Format24bppRgb:
                    {
                        Compare24bppRgb(aPixelSize, aRow, bRow, rRow, x);
                    }
                    break;
                case PixelFormat.Format32bppRgb:
                    break;
                case PixelFormat.Format32bppArgb:
                    {
                        Compare32bppArgb(aPixelSize, aRow, bRow, rRow, x);
                    }
                    break;
                case PixelFormat.Format32bppPArgb:
                    break;
                case PixelFormat.Format48bppRgb:
                    break;
                case PixelFormat.Format64bppArgb:
                    break;
                case PixelFormat.Format64bppPArgb:
                    break;
                case PixelFormat.Max:
                    break;
                default:
                    break;
            }
        }

        private unsafe void Compare32bppArgb(int aPixelSize, byte* aRow, byte* bRow, byte* rRow, int x)
        {
            //RGB: bgra (0,1,2,3) 8x8x8x8
            byte aA = aRow[x * aPixelSize + 3];
            byte rA = aRow[x * aPixelSize + 2];
            byte gA = aRow[x * aPixelSize + 1];
            byte bA = aRow[x * aPixelSize];

            byte aB = aRow[x * aPixelSize + 3];
            byte rB = bRow[x * aPixelSize + 2];
            byte gB = bRow[x * aPixelSize + 1];
            byte bB = bRow[x * aPixelSize];

            double de = Math.Sqrt((rB - rA) * (rB - rA) + (gB - gA) * (gB - gA) + (bB - bA) * (bB - bA)) / Math.Sqrt(Byte.MaxValue * Byte.MaxValue * 3);
            if (de < mu_tolerance)
                de = 0.0;

            byte rR = Convert.ToByte(byte.MaxValue - Convert.ToByte(Math.Round(de * Byte.MaxValue)));

            byte gR = rR;
            byte bR = rR;
            byte aR = Byte.MaxValue;

            rRow[x * aPixelSize] = bR;
            rRow[x * aPixelSize + 1] = gR;
            rRow[x * aPixelSize + 2] = rR;
            rRow[x * aPixelSize + 3] = aR;
        }

        private unsafe void Compare24bppRgb(int aPixelSize, byte* aRow, byte* bRow, byte* rRow, int x)
        {
            //RGB: bgr (0,1,2) 8x8x8
            byte rA = aRow[x * aPixelSize + 2];
            byte gA = aRow[x * aPixelSize + 1];
            byte bA = aRow[x * aPixelSize];

            byte rB = bRow[x * aPixelSize + 2];
            byte gB = bRow[x * aPixelSize + 1];
            byte bB = bRow[x * aPixelSize];

            double de = Math.Sqrt((rB - rA) * (rB - rA) + (gB - gA) * (gB - gA) + (bB - bA) * (bB - bA));

            if (de < mu_tolerance)
                de = 0.0;

            byte rR = Convert.ToByte(byte.MaxValue - Convert.ToByte(Math.Round(de * Byte.MaxValue)));

            byte gR = rR;
            byte bR = rR;

            rRow[x * aPixelSize] = bR;
            rRow[x * aPixelSize + 1] = gR;
            rRow[x * aPixelSize + 2] = rR;
        }

        public Bitmap MedianFilter(Bitmap sourceBitmap,
                                    int matrixSize,
                                    bool grayscale = false)
        {
            BitmapData sourceData =
                       sourceBitmap.LockBits(new Rectangle(0, 0,
                       sourceBitmap.Width, sourceBitmap.Height),
                       ImageLockMode.ReadOnly,
                       PixelFormat.Format32bppArgb);


            byte[] pixelBuffer = new byte[sourceData.Stride *
                                          sourceData.Height];


            byte[] resultBuffer = new byte[sourceData.Stride *
                                           sourceData.Height];


            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0,
                                       pixelBuffer.Length);


            sourceBitmap.UnlockBits(sourceData);


            if (grayscale == true)
            {
                float rgb = 0;


                for (int k = 0; k < pixelBuffer.Length; k += 4)
                {
                    rgb = pixelBuffer[k] * 0.11f;
                    rgb += pixelBuffer[k + 1] * 0.59f;
                    rgb += pixelBuffer[k + 2] * 0.3f;


                    pixelBuffer[k] = (byte)rgb;
                    pixelBuffer[k + 1] = pixelBuffer[k];
                    pixelBuffer[k + 2] = pixelBuffer[k];
                    pixelBuffer[k + 3] = 255;
                }
            }


            int filterOffset = (matrixSize - 1) / 2;
            int calcOffset = 0;


            int byteOffset = 0;

            List<int> neighbourPixels = new List<int>();
            byte[] middlePixel;


            for (int offsetY = filterOffset; offsetY <
                sourceBitmap.Height - filterOffset; offsetY++)
            {
                for (int offsetX = filterOffset; offsetX <
                    sourceBitmap.Width - filterOffset; offsetX++)
                {
                    byteOffset = offsetY *
                                 sourceData.Stride +
                                 offsetX * 4;


                    neighbourPixels.Clear();


                    for (int filterY = -filterOffset;
                        filterY <= filterOffset; filterY++)
                    {
                        for (int filterX = -filterOffset;
                            filterX <= filterOffset; filterX++)
                        {


                            calcOffset = byteOffset +
                                         (filterX * 4) +
                                (filterY * sourceData.Stride);


                            neighbourPixels.Add(BitConverter.ToInt32(
                                             pixelBuffer, calcOffset));
                        }
                    }


                    neighbourPixels.Sort();

                    middlePixel = BitConverter.GetBytes(
                                       neighbourPixels[filterOffset]);


                    resultBuffer[byteOffset] = middlePixel[0];
                    resultBuffer[byteOffset + 1] = middlePixel[1];
                    resultBuffer[byteOffset + 2] = middlePixel[2];
                    resultBuffer[byteOffset + 3] = middlePixel[3];
                }
            }


            Bitmap resultBitmap = new Bitmap(sourceBitmap.Width,
                                             sourceBitmap.Height);


            BitmapData resultData =
                       resultBitmap.LockBits(new Rectangle(0, 0,
                       resultBitmap.Width, resultBitmap.Height),
                       ImageLockMode.WriteOnly,
                       PixelFormat.Format32bppArgb);


            Marshal.Copy(resultBuffer, 0, resultData.Scan0,
                                       resultBuffer.Length);


            resultBitmap.UnlockBits(resultData);


            return resultBitmap;
        }

        #region SAMPLE
        Bitmap MakeBW(Bitmap source)
        {
            //использование промежуточных переменных ускоряет 
            //код в несколько раз
            var width = source.Width;
            var height = source.Height;

            var sourceData = source.LockBits(new Rectangle(new System.Drawing.Point(0, 0), source.Size),
             ImageLockMode.ReadOnly,
             source.PixelFormat);

            var result = new Bitmap(width, height, source.PixelFormat);
            var resultData = result.LockBits(new Rectangle(new System.Drawing.Point(0, 0), result.Size),
             ImageLockMode.ReadWrite,
             source.PixelFormat);


            var sourceStride = sourceData.Stride;
            var resultStride = resultData.Stride;

            var sourceScan0 = sourceData.Scan0;
            var resultScan0 = resultData.Scan0;

            var resultPixelSize = resultStride / width;

            unsafe
            {
                for (var y = 0; y < height; y++)
                {
                    var sourceRow = (byte*)sourceScan0 + (y * sourceStride);
                    var resultRow = (byte*)resultScan0 + (y * resultStride);
                    for (var x = 0; x < width; x++)
                    {
                        //r g b
                        var v = (byte)(0.3 * sourceRow[x * resultPixelSize + 2] + 0.59 * sourceRow[x * resultPixelSize + 1] +
                         0.11 * sourceRow[x * resultPixelSize]);
                        resultRow[x * resultPixelSize] = v;
                        resultRow[x * resultPixelSize + 1] = v;
                        resultRow[x * resultPixelSize + 2] = v;
                    }
                }


            }

            source.UnlockBits(sourceData);
            result.UnlockBits(resultData);
            return result;
        }

        #endregion

        private void muToleranceMaskedTextBox_Validated(object sender, EventArgs e)
        {
            mu_tolerance = Convert.ToDouble(muToleranceMaskedTextBox.Text);
        }

        private void useMedianFilterCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            useMedianFilter = useMedianFilterCheckBox.Checked;
            EnableMedianFilterControls(useMedianFilter);
        }

        private void EnableMedianFilterControls(bool enabled)
        {
            matrixSizeLabel.Enabled = enabled;
            matrixSizeMaskedTextBox.Enabled = enabled;
            greyScaleCheckBox.Enabled = enabled;
        }

        private void matrixSizeMaskedTextBox_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void matrixSizeMaskedTextBox_Validated(object sender, EventArgs e)
        {
            matrixSize = Convert.ToInt16( matrixSizeMaskedTextBox.Text );
        }


        private void greyScaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            greyScale = greyScaleCheckBox.Checked;
        }
    }
}
