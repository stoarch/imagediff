﻿namespace ImageDiff
{
    partial class ImageDiffForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstPictureBox = new System.Windows.Forms.PictureBox();
            this.secondPictureBox = new System.Windows.Forms.PictureBox();
            this.loadFirstButton = new System.Windows.Forms.Button();
            this.loadSecondButton = new System.Windows.Forms.Button();
            this.compareButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.resultPictureBox = new System.Windows.Forms.PictureBox();
            this.muToleranceMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.useMedianFilterCheckBox = new System.Windows.Forms.CheckBox();
            this.matrixSizeLabel = new System.Windows.Forms.Label();
            this.matrixSizeMaskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.greyScaleCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.firstPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // firstPictureBox
            // 
            this.firstPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.firstPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.firstPictureBox.Location = new System.Drawing.Point(16, 15);
            this.firstPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.firstPictureBox.Name = "firstPictureBox";
            this.firstPictureBox.Size = new System.Drawing.Size(561, 436);
            this.firstPictureBox.TabIndex = 0;
            this.firstPictureBox.TabStop = false;
            // 
            // secondPictureBox
            // 
            this.secondPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.secondPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.secondPictureBox.Location = new System.Drawing.Point(585, 15);
            this.secondPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.secondPictureBox.Name = "secondPictureBox";
            this.secondPictureBox.Size = new System.Drawing.Size(561, 436);
            this.secondPictureBox.TabIndex = 1;
            this.secondPictureBox.TabStop = false;
            // 
            // loadFirstButton
            // 
            this.loadFirstButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadFirstButton.Location = new System.Drawing.Point(16, 474);
            this.loadFirstButton.Margin = new System.Windows.Forms.Padding(4);
            this.loadFirstButton.Name = "loadFirstButton";
            this.loadFirstButton.Size = new System.Drawing.Size(140, 51);
            this.loadFirstButton.TabIndex = 2;
            this.loadFirstButton.Text = "Load first";
            this.loadFirstButton.UseVisualStyleBackColor = true;
            this.loadFirstButton.Click += new System.EventHandler(this.loadFirstButton_Click);
            // 
            // loadSecondButton
            // 
            this.loadSecondButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.loadSecondButton.Location = new System.Drawing.Point(164, 474);
            this.loadSecondButton.Margin = new System.Windows.Forms.Padding(4);
            this.loadSecondButton.Name = "loadSecondButton";
            this.loadSecondButton.Size = new System.Drawing.Size(140, 51);
            this.loadSecondButton.TabIndex = 3;
            this.loadSecondButton.Text = "Load second";
            this.loadSecondButton.UseVisualStyleBackColor = true;
            this.loadSecondButton.Click += new System.EventHandler(this.loadSecondButton_Click);
            // 
            // compareButton
            // 
            this.compareButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.compareButton.Location = new System.Drawing.Point(312, 474);
            this.compareButton.Margin = new System.Windows.Forms.Padding(4);
            this.compareButton.Name = "compareButton";
            this.compareButton.Size = new System.Drawing.Size(140, 51);
            this.compareButton.TabIndex = 4;
            this.compareButton.Text = "Compare";
            this.compareButton.UseVisualStyleBackColor = true;
            this.compareButton.Click += new System.EventHandler(this.compareButton_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "PNG files|*.png";
            // 
            // resultPictureBox
            // 
            this.resultPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.resultPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.resultPictureBox.Location = new System.Drawing.Point(1154, 15);
            this.resultPictureBox.Margin = new System.Windows.Forms.Padding(4);
            this.resultPictureBox.Name = "resultPictureBox";
            this.resultPictureBox.Size = new System.Drawing.Size(561, 436);
            this.resultPictureBox.TabIndex = 5;
            this.resultPictureBox.TabStop = false;
            // 
            // muToleranceMaskedTextBox
            // 
            this.muToleranceMaskedTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.muToleranceMaskedTextBox.Location = new System.Drawing.Point(475, 474);
            this.muToleranceMaskedTextBox.Mask = "#.###";
            this.muToleranceMaskedTextBox.Name = "muToleranceMaskedTextBox";
            this.muToleranceMaskedTextBox.Size = new System.Drawing.Size(114, 22);
            this.muToleranceMaskedTextBox.TabIndex = 6;
            this.muToleranceMaskedTextBox.Text = "01";
            this.muToleranceMaskedTextBox.Validated += new System.EventHandler(this.muToleranceMaskedTextBox_Validated);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(472, 454);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Mu tolerance:";
            // 
            // useMedianFilterCheckBox
            // 
            this.useMedianFilterCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.useMedianFilterCheckBox.AutoSize = true;
            this.useMedianFilterCheckBox.Location = new System.Drawing.Point(609, 458);
            this.useMedianFilterCheckBox.Name = "useMedianFilterCheckBox";
            this.useMedianFilterCheckBox.Size = new System.Drawing.Size(247, 21);
            this.useMedianFilterCheckBox.TabIndex = 8;
            this.useMedianFilterCheckBox.Text = "Use median filter (noise reduction)";
            this.useMedianFilterCheckBox.UseVisualStyleBackColor = true;
            this.useMedianFilterCheckBox.CheckedChanged += new System.EventHandler(this.useMedianFilterCheckBox_CheckedChanged);
            // 
            // matrixSizeLabel
            // 
            this.matrixSizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.matrixSizeLabel.AutoSize = true;
            this.matrixSizeLabel.Enabled = false;
            this.matrixSizeLabel.Location = new System.Drawing.Point(606, 483);
            this.matrixSizeLabel.Name = "matrixSizeLabel";
            this.matrixSizeLabel.Size = new System.Drawing.Size(78, 17);
            this.matrixSizeLabel.TabIndex = 10;
            this.matrixSizeLabel.Text = "Matrix size:";
            // 
            // matrixSizeMaskedTextBox
            // 
            this.matrixSizeMaskedTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.matrixSizeMaskedTextBox.Enabled = false;
            this.matrixSizeMaskedTextBox.Location = new System.Drawing.Point(609, 503);
            this.matrixSizeMaskedTextBox.Mask = "##";
            this.matrixSizeMaskedTextBox.Name = "matrixSizeMaskedTextBox";
            this.matrixSizeMaskedTextBox.Size = new System.Drawing.Size(75, 22);
            this.matrixSizeMaskedTextBox.TabIndex = 9;
            this.matrixSizeMaskedTextBox.Text = "3";
            this.matrixSizeMaskedTextBox.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.matrixSizeMaskedTextBox_MaskInputRejected);
            this.matrixSizeMaskedTextBox.Validated += new System.EventHandler(this.matrixSizeMaskedTextBox_Validated);
            // 
            // greyScaleCheckBox
            // 
            this.greyScaleCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.greyScaleCheckBox.AutoSize = true;
            this.greyScaleCheckBox.Enabled = false;
            this.greyScaleCheckBox.Location = new System.Drawing.Point(690, 503);
            this.greyScaleCheckBox.Name = "greyScaleCheckBox";
            this.greyScaleCheckBox.Size = new System.Drawing.Size(98, 21);
            this.greyScaleCheckBox.TabIndex = 13;
            this.greyScaleCheckBox.Text = "Grey scale";
            this.greyScaleCheckBox.UseVisualStyleBackColor = true;
            this.greyScaleCheckBox.Visible = false;
            this.greyScaleCheckBox.CheckedChanged += new System.EventHandler(this.greyScaleCheckBox_CheckedChanged);
            // 
            // ImageDiffForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1806, 539);
            this.Controls.Add(this.greyScaleCheckBox);
            this.Controls.Add(this.matrixSizeLabel);
            this.Controls.Add(this.matrixSizeMaskedTextBox);
            this.Controls.Add(this.useMedianFilterCheckBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.muToleranceMaskedTextBox);
            this.Controls.Add(this.resultPictureBox);
            this.Controls.Add(this.compareButton);
            this.Controls.Add(this.loadSecondButton);
            this.Controls.Add(this.loadFirstButton);
            this.Controls.Add(this.secondPictureBox);
            this.Controls.Add(this.firstPictureBox);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ImageDiffForm";
            this.Text = "Image comparer";
            ((System.ComponentModel.ISupportInitialize)(this.firstPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox firstPictureBox;
        private System.Windows.Forms.PictureBox secondPictureBox;
        private System.Windows.Forms.Button loadFirstButton;
        private System.Windows.Forms.Button loadSecondButton;
        private System.Windows.Forms.Button compareButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.PictureBox resultPictureBox;
        private System.Windows.Forms.MaskedTextBox muToleranceMaskedTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox useMedianFilterCheckBox;
        private System.Windows.Forms.Label matrixSizeLabel;
        private System.Windows.Forms.MaskedTextBox matrixSizeMaskedTextBox;
        private System.Windows.Forms.CheckBox greyScaleCheckBox;
    }
}

